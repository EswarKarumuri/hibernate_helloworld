package org.hibernate.tutorial;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.tutorial.domain.Event;
import org.hibernate.tutorial.domain.Person;

public class EventManager {
	public static void main(String[] args) {
		EventManager mgr = new EventManager();
		//mgr.createAndStoreEvent("My Event", new Date());
		//mgr.createAndStorePerson("Eswar", "Karumuri", 27);
		mgr.addPersonToEvent(mgr.createAndStorePerson("Eswar", "Karumuri", 27), mgr.createAndStoreEvent("My Event", new Date()));
		HibernateUtil.getSessionFactory().close();
	}

	private Long createAndStoreEvent(String title, Date theDate) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		Event theEvent = new Event();
		theEvent.setTitle(title);
		theEvent.setDate(theDate);
		Long id = (Long)session.save(theEvent);

		session.getTransaction().commit();
		return id;
		
	}
	
	private Long createAndStorePerson(String firstName, String lastName, int age) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		Person thePerson = new Person();
		thePerson.setAge(age);
		thePerson.setFirstname(firstName);
		thePerson.setLastname(lastName);
		Long id = (Long) session.save(thePerson);

		session.getTransaction().commit();
		return id;
	}
	
	private void addPersonToEvent(Long personId, Long eventId){
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Person person = (Person)session.load(Person.class, personId);
		Event event = (Event)session.load(Event.class, eventId);
		person.getEvents().add(event);
		
		session.getTransaction().commit();
	}
}
 